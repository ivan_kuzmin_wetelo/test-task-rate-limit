import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { InjectRedis } from '@liaoliaots/nestjs-redis';
import Redis from 'ioredis';
import * as moment from 'moment';
import { Reflector } from '@nestjs/core';
import { IGuardMetadata } from '../interfaces/IGuardMetadata';

const INTERVAL_LIMIT_HOURS = 1;
const DEFAULT_REQUEST_WEIGHT = 1;
export class RateLimitGuard implements CanActivate {
  constructor(
    @InjectRedis() private readonly redis: Redis,
    private reflector: Reflector,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const metadata = this.reflector.get<IGuardMetadata>(
      'metadataObj',
      context.getHandler(),
    );
    const requestWeight = metadata.requestWeight || DEFAULT_REQUEST_WEIGHT;
    const request = context.switchToHttp().getRequest();
    let redisKey = request.headers['x-access-token'];
    let requestLimit = process.env.PRIVATE_REQUEST_LIMIT;
    if (metadata.isPublic) {
      redisKey = request.ip;
      requestLimit = process.env.PUBLIC_REQUEST_LIMIT;
    }
    if (!redisKey) {
      return false;
    }
    const requestLog = await this.redis.get(redisKey);
    if (!requestLog) {
      //create new request log
      await this.redis.set(
        redisKey,
        JSON.stringify({
          requestTime: moment().unix(),
          counter: requestWeight,
        }),
      );
    } else {
      //increment request counter
      let counter = JSON.parse(requestLog).counter;
      counter += requestWeight;
      const firstRequestTimestamp = JSON.parse(requestLog).requestTime;
      const nowTime = moment();
      let newRequestTimestamp = firstRequestTimestamp;
      // if difference between current timestamp and firstRequestTimestamp,
      // more than firstRequestTimestamp, then update request time and counter
      if (
        nowTime.diff(moment(firstRequestTimestamp, 'X'), 'hours', true) >
        INTERVAL_LIMIT_HOURS
      ) {
        newRequestTimestamp = moment().unix();
        counter = 0;
      }
      //check the limit
      if (counter >= requestLimit) {
        const nextRequestTime = moment(firstRequestTimestamp, 'X').add(
          INTERVAL_LIMIT_HOURS,
          'hours',
        );
        throw new HttpException(
          'Too many request. You can send the next request at ' +
            nextRequestTime.format('YYYY-MM-DD HH:mm:ss'),
          HttpStatus.TOO_MANY_REQUESTS,
        );
      }
      await this.redis.set(
        redisKey,
        JSON.stringify({
          requestTime: newRequestTimestamp,
          counter: counter,
        }),
      );
    }
    return true;
  }
}
