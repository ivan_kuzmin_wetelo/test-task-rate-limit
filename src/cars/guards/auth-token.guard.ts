import { CanActivate, ExecutionContext } from '@nestjs/common';

export class AuthTokenGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    return request.headers['x-access-token'] === process.env.X_ACCESS_TOKEN;
  }
}
