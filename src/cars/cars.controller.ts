import { Controller, Get, UseGuards } from '@nestjs/common';
import { CarsService } from './cars.service';
import { AuthTokenGuard, RateLimitGuard } from './guards';
import { GuardMetadata } from './decorators/guard-metadata.decorator';

const PUBLIC_REQUEST_WEIGHT = 5;
const PRIVATE_REQUEST_WEIGHT = 1;
@Controller('cars')
export class CarsController {
  constructor(private readonly carsService: CarsService) {}

  @Get('/public')
  @UseGuards(RateLimitGuard)
  @GuardMetadata({ isPublic: true, requestWeight: PUBLIC_REQUEST_WEIGHT })
  findAll() {
    return this.carsService.findPublicAll();
  }

  @Get('/private')
  @UseGuards(AuthTokenGuard, RateLimitGuard)
  @GuardMetadata({ isPublic: false, requestWeight: PRIVATE_REQUEST_WEIGHT })
  findOne() {
    return this.carsService.findPrivateAll();
  }
}
