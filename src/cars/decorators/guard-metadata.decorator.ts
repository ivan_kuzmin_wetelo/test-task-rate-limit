import { SetMetadata } from '@nestjs/common';
import { IGuardMetadata } from '../interfaces/IGuardMetadata';

export const GuardMetadata = (metadataObj: IGuardMetadata) =>
  SetMetadata('metadataObj', metadataObj);
