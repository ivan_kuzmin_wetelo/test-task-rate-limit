import { Injectable } from '@nestjs/common';

@Injectable()
export class CarsService {
  findPublicAll() {
    return [
      {
        id: 1,
        name: 'Audi',
      },
      {
        id: 2,
        name: 'BMW',
      },
      {
        id: 3,
        name: 'Mercedes',
      },
    ];
  }

  findPrivateAll() {
    return [
      {
        id: 1,
        name: 'Ferrari',
      },
      {
        id: 2,
        name: 'lamborghini',
      },
    ];
  }
}
