export interface IGuardMetadata {
  isPublic: boolean;
  requestWeight: number;
}
