To run this application locally do the following
1) setup nodejs (16.9.1) and docker
2) create .env file from .env.example
3) npm install
4) Run redis
   docker run --name my-redis -p 6379:6379 -d redis
5) Run application 
   npm run start

User token and limits are saved in the .env file

Routes for testing
localhost:3000/cars/private - Request should contain x-access-token header, request weight equals 5
localhost:3000/cars/public - request weight equals 1
